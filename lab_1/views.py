from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Firdhan Hilmy Purnomo'  # TODO Implement this
name_right = 'Ananda Daffa'
name_left = 'Kursi'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1998, 12, 8) #TODO Implement this, format (Year, Month, Date)
npm = 1706026992 # TODO Implement this
npm_right = 1706074676
npm_left = '-'
kampus = "Universitas Indonesia"
hobi = "Memancing"
hobi_right = "Main gacha"
hobi_left = 'Didudukin'
deskripsi = "S: Mudah termotivasi | W: Kurang bisa konsisten | O: Orang tua yang sangat baik | T: Makanan yang menggoda"
deskripsi_right = "Seorang mahasiswa Fasilkom UI"
deskripsi_left = 'Kursi Lab 1103 Fasilkom UI'

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'kampus': kampus, 
    'hobi': hobi, 'deskripsi': deskripsi, 'name_right': name_right, 'npm_right': npm_right,
    'hobi_right': hobi_right, 'deskripsi_right': deskripsi_right, 'name_left': name_left, 'npm_left': npm_left,
    'hobi_left': hobi_left, 'deskripsi_left': deskripsi_left}
    return render(request, 'story1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
